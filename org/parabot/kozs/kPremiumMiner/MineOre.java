package org.parabot.kozs.kPremiumMiner;

import org.parabot.environment.api.utils.Time;
import org.parabot.environment.scripts.framework.Strategy;
import org.rev317.min.api.methods.Inventory;
import org.rev317.min.api.methods.Players;
import org.rev317.min.api.methods.SceneObjects;
import org.rev317.min.api.wrappers.SceneObject;

public class MineOre implements Strategy {
	
	
	public static int rock1,rock2;
	public int[] rocks = {rock1, rock2};
	
	
	@Override
	public boolean activate() {
		return !Inventory.isFull();
	}

	@Override
	public void execute() {
		for (SceneObject i : SceneObjects.getNearest(rocks)) {
			if (Players.getMyPlayer().getAnimation() == -1) {
				if (i.distanceTo() <= Main.distance) {
					if (!Inventory.isFull()) {
					i.interact(0);
					Time.sleep(3000);
					}
				}
			}
			while (Players.getMyPlayer().getAnimation() != -1) {
				Time.sleep(1000);
			}
		}
	}
}
