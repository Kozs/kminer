package org.parabot.kozs.kPremiumMiner;

import java.awt.*;

import org.rev317.min.api.methods.Skill;

/**
 * User: Fryslan
 */
public class Progressbar {

	private Color color;
	private Color borderColor;
	private int x;
	private int y;
	private int width;
	private int height;
	private int percentage;

	public Progressbar(int x, int y, int width, int height, int percentage) {

		this.color = Color.BLUE;
		this.borderColor = Color.BLUE;
		this.x = x;
		this.y = y;
		this.width = width;
		this.height = height;
		this.percentage = percentage;
	}

	public Progressbar(Color color, int x, int y, int width, int height, int percentage) {

		this.color = color;
		this.borderColor = color;
		this.x = x;
		this.y = y;
		this.width = width;
		this.height = height;
		this.percentage = percentage;
	}

	public Progressbar(Color color, Color borderColor, int x, int y, int width, int height, int percentage) {

		this.color = color;
		this.borderColor = borderColor;
		this.x = x;
		this.y = y;
		this.width = width;
		this.height = height;
		this.percentage = percentage;
	}

	/**
	 * Draw the Progressbar.
	 */
    private final Font font3 = new Font("Comic Sans MS", 0, 12);
	public void draw(Graphics graphics) {
		graphics.setColor(borderColor);
		graphics.drawRect(x, y, width, height);
		graphics.setColor(color);
		graphics.fillRect(x + 1, y + 1, ((width / 100) * percentage) - 1, height - 1);
		graphics.setFont(font3);
		graphics.setColor(Color.WHITE);
        graphics.drawString("Mining Level: " + Skill.MINING.getLevel(), 232, 334);
	}

	public Color getColor() {
		return color;
	}

	public void setColor(Color color) {
		this.color = color;
	}

	public Color getBorderColor() {
		return borderColor;
	}

	public void setBorderColor(Color borderColor) {
		this.borderColor = borderColor;
	}

	public int getX() {
		return x;
	}

	public void setX(int x) {
		this.x = x;
	}

	public int getY() {
		return y;
	}

	public void setY(int y) {
		this.y = y;
	}

	public int getWidth() {
		return width;
	}

	public void setWidth(int width) {
		this.width = width;
	}

	public int getHeight() {
		return height;
	}

	public void setHeight(int height) {
		this.height = height;
	}

	public int getPercentage() {
		return percentage;
	}

	public void setPercentage(int percentage) {
		this.percentage = percentage;
	}
}
