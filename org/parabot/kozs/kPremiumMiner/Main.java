package org.parabot.kozs.kPremiumMiner;

import java.awt.BasicStroke;
import java.awt.Color;
import java.awt.Component;
import java.awt.EventQueue;
import java.awt.Font;
import java.awt.Graphics;
import java.awt.Graphics2D;
import java.awt.Image;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.io.IOException;
import java.net.URL;
import java.util.ArrayList;

import javax.imageio.ImageIO;
import javax.swing.DefaultComboBoxModel;
import javax.swing.JButton;
import javax.swing.JComboBox;
import javax.swing.JFrame;
import javax.swing.JLabel;
import javax.swing.JOptionPane;
import javax.swing.JPanel;
import javax.swing.border.EmptyBorder;

import org.parabot.environment.api.interfaces.Paintable;
import org.parabot.environment.scripts.Category;
import org.parabot.environment.scripts.Script;
import org.parabot.environment.scripts.ScriptManifest;
import org.parabot.environment.scripts.framework.Strategy;
import org.rev317.min.api.methods.Inventory;
import org.rev317.min.api.methods.Skill;

@ScriptManifest(author = "Kozs", category = Category.MINING,
description = "AIOMiner for PKH", name = "kPremMiner", servers = { "PKHonor" }, version = 4.0)

public class Main extends Script implements Paintable {
	public static int ores, currentOres;
	Timer t = new Timer();
	ArrayList<Strategy> strat = new ArrayList<Strategy>();
	GUI g = new GUI();
	public boolean guiWait = true;
	public static int distance = 10;
	static int startAmount;
	static int inventoryAmount;
	static int currentAmount;
	static int gainedAmount;
	static int product;
	
	@SuppressWarnings("static-access")
	public boolean onExecute() {
		startAmount = Inventory.getCount(true, product);
		t.startTime = System.currentTimeMillis();
		//Executes when Script is started
		g.setVisible(true);
		while (guiWait == true) {
			sleep(500);
		}
		strat.add(new MineOre());
		strat.add(new BankOre());
		provide(strat);
		return true;
	}
	
	public void onFinish() {
		//Executes when Script ends
	//openWebPage("www.google.com");
	}

    private Image getImage(String url) {
        try {
            return ImageIO.read(new URL(url));
        } catch(IOException e) {
            return null;
        }
    }
    public void openWebPage(String url){
    	   try {         
    	     java.awt.Desktop.getDesktop().browse(java.net.URI.create(url));
    	   }
    	   catch (java.io.IOException e) {
    	       System.out.println(e.getMessage());
    	   }
    	}

    private final Color color1 = new Color(6, 248, 248, 76);
    private final Color color2 = new Color(0, 0, 0);

    private final BasicStroke stroke1 = new BasicStroke(1);

    Progressbar bar = new Progressbar(Color.BLUE,Color.black,3,316,511,20,100);
    private final Font font1 = new Font("Comic Sans MS", 0, 17);
    private final Font font2 = new Font("Comic Sans MS", 0, 10);
    private final Font font3 = new Font("Comic Sans MS", 0, 9);
    private final Image img1 = getImage("http://images2.wikia.nocookie.net/__cb20091205044857/runescape/images/archive/2/28/20110305004636!Mining-icon.png");
    @SuppressWarnings("static-access")
	public void paint(Graphics g1) {
    	if (Skill.MINING.getLevel() == 99) {
        	bar.setPercentage(102);
    	} else {
        	bar.setPercentage(Skill.MINING.getLevel() + 2);
    	}

        bar.draw(g1);
    	gainedAmount = startAmount + currentAmount + inventoryAmount;
    	inventoryAmount = Inventory.getCount(true, product);
        Graphics2D g = (Graphics2D)g1;
        g.setColor(color1);
        g.fillRoundRect(2, 2, 183, 103, 16, 16);
        g.setColor(color2);
        g.setStroke(stroke1);
        g.drawRoundRect(2, 2, 183, 103, 16, 16);
        g.drawImage(img1, 18, 20, null);
        g.drawImage(img1, 144, 20, null);
        g.setFont(font1);
        g.drawString("kMiner", 64, 41);
        g.setFont(font2);
        g.drawString("Time: "+t.runTime(t.startTime), 18, 64);
        g.drawString("Ores Mined (Hr): " +gainedAmount+" ("+ t.perHour(gainedAmount)+")", 19, 77);
        g.setFont(font3);
        g.drawString("Version: 4.1", 124, 101);
    }
	public class GUI extends JFrame {
		/**
		 * 
		 */
		private static final long serialVersionUID = -5179763579956978220L;
		private JPanel contentPane;

		/**
		 * Launch the application.
		 */
		public void main(String[] args) {
			EventQueue.invokeLater(new Runnable() {
				public void run() {
					try {
						GUI frame = new GUI();
						frame.setVisible(true);
					} catch (Exception e) {
						e.printStackTrace();
					}
				}
			});
		}

		/**
		 * Create the frame.
		 * 
		 * @return
		 */
		@SuppressWarnings({ "unchecked", "rawtypes" })
		public GUI() {
			setDefaultCloseOperation(JFrame.DISPOSE_ON_CLOSE);
			setBounds(100, 100, 141, 160);
			contentPane = new JPanel();
			contentPane.setBorder(new EmptyBorder(5, 5, 5, 5));
			setContentPane(contentPane);
			contentPane.setLayout(null);
			setResizable(false);
			final JComboBox comboBox = new JComboBox();
			comboBox.setModel(new DefaultComboBoxModel(new String[] { "Tin", "Copper", "Iron", 
																	  "Gold", "Mithril", "Adamanite",
																	  "Runite", "Coal" }));
			comboBox.setBounds(10, 56, 105, 20);
			contentPane.add(comboBox);

			JLabel lblPickAFish = new JLabel("Rocks!");
			lblPickAFish.setFont(new Font("Trebuchet MS", Font.PLAIN, 13));
			lblPickAFish.setBounds(29, 11, 66, 34);
			contentPane.add(lblPickAFish);

			JButton startBtn = new JButton("Start!\r\n");
			startBtn.addActionListener(new ActionListener() {
				private Component frame;

				public void actionPerformed(ActionEvent e) {
					@SuppressWarnings("unused")
					String chosen = comboBox.getSelectedItem().toString();
					if (comboBox.getSelectedItem().equals("Runite")) {
						MineOre.rock1 = 2107;
						MineOre.rock2 = 2106;
						product = 452;
					}
					if (comboBox.getSelectedItem().equals("Copper")) {
						MineOre.rock1 = 2091;
						MineOre.rock2 = 2090;
						product = 437;
					}
					if (comboBox.getSelectedItem().equals("Tin")) {
						MineOre.rock1 = 2095;
						MineOre.rock2 = 2094;
						product = 439;
					}
					if (comboBox.getSelectedItem().equals("Adamanite")) {
						MineOre.rock1 = 2105;
						MineOre.rock2 = 2104;
						product = 450;
					}
					if (comboBox.getSelectedItem().equals("Mithril")) {
						MineOre.rock1 = 2103;
						MineOre.rock2 = 2102;
						product = 452;
					}
					if (comboBox.getSelectedItem().equals("Iron")) {
						MineOre.rock1 = 2092;
						MineOre.rock2 = 2093;
						product = 441;
					}
					if (comboBox.getSelectedItem().equals("Coal")) {
						MineOre.rock1 = 2097;
						MineOre.rock2 = 2096;
						product = 454;
					}
					if (comboBox.getSelectedItem().equals("Gold")) {
						MineOre.rock1 = 2098;
						MineOre.rock2 = 2099;
						product = 445;
						distance = 25;
					}
					JOptionPane.showMessageDialog(frame,
						    "Please consider leaving FB on Forums.",
						    "kPremiumMiner",
						    JOptionPane.WARNING_MESSAGE);
					guiWait = false;
					g.dispose();
				}
			});
			startBtn.setBounds(10, 87, 105, 23);
			contentPane.add(startBtn);
		}
	}

}
