package org.parabot.kozs.kPremiumMiner;

import java.text.DecimalFormat;
import java.util.concurrent.TimeUnit;

public class Timer {
		static long startTime;
	    static long StartTime;
	    
	    public static String perHour(int gained) {
	        return formatNumber((int) ((gained) * 3600000D / (System.currentTimeMillis() - startTime)));
	}
        public static int CustomSleepTime(int amount) {
            return amount * 1000;
    }
	 
	public static String formatNumber(int start) {
	DecimalFormat nf = new DecimalFormat("0.0");
	double i = start;
	if(i >= 1000000) {
	    return nf.format((i / 1000000)) + "m";
	}
	if(i >=  1000) {
	    return nf.format((i / 1000)) + "k";
	}
	return ""+start;
	}
	 
	public static String runTime(long i) {
	DecimalFormat nf = new DecimalFormat("00");
	long millis = System.currentTimeMillis() - i;
	long hours = millis / (1000 * 60 * 60);
	millis -= hours * (1000 * 60 * 60);
	long minutes = millis / (1000 * 60);
	millis -= minutes * (1000 * 60);
	long seconds = millis / 1000;
	return nf.format(hours) + ":" + nf.format(minutes) + ":" + nf.format(seconds);
	}

	/**
	 * Returns string in a HH:MM:SS format
	 */
	public String getTimeRan() {
	        final long currentTime = System.currentTimeMillis() - StartTime;
	        String format = String.format(
	                        "%02d:%02d:%02d",
	                        TimeUnit.MILLISECONDS.toHours(currentTime),
	                        TimeUnit.MILLISECONDS.toMinutes(currentTime)
	                                        - TimeUnit.HOURS.toMinutes(TimeUnit.MILLISECONDS
	                                                        .toHours(currentTime)),
	                        TimeUnit.MILLISECONDS.toSeconds(currentTime)
	                                        - TimeUnit.MINUTES.toSeconds(TimeUnit.MILLISECONDS
	                                                        .toMinutes(currentTime)));
	        return format;
	}
	    

	}
