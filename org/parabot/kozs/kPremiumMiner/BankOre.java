package org.parabot.kozs.kPremiumMiner;

import org.parabot.environment.api.utils.Time;
import org.parabot.environment.scripts.framework.Strategy;
import org.rev317.min.api.methods.Game;
import org.rev317.min.api.methods.Inventory;
import org.rev317.min.api.methods.Menu;
import org.rev317.min.api.methods.SceneObjects;
import org.rev317.min.api.wrappers.SceneObject;

public class BankOre implements Strategy {

	@Override
	public boolean activate() {
		return Inventory.isFull();
	}

	@Override
	public void execute() {
		for (SceneObject i : SceneObjects.getNearest(Bank.BANKS)) {
			if (Inventory.isFull() && i != null) {
				while (Game.getOpenInterfaceId() == -1) {
					i.interact(0);
					Time.sleep(1000);
				}
				if (Game.getOpenInterfaceId() == 23350) {
					depositAll(1000);
					Bank.close();
				}
			}
		}
	}

	public void depositAll(int sleep) {
		if (Inventory.isFull()) {
			Main.currentAmount += Inventory.getCount(true, Main.product);
			Menu.sendAction(646, 438, 35, 23412);
			Time.sleep(sleep);
		}
	}
}
